# project to manage personal email (project-email-quasar)

A Quasar Project

## Install the dependencies

```bash
yarn
# or
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
quasar dev
```

### Lint the files

```bash
yarn lint
# or
npm run lint
```

### Format the files

```bash
yarn format
# or
npm run format
```

### Build the app for production

```bash
quasar build
```

### Customize the configuration

See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-webpack/quasar-config-js).

### Información general

Proyecto que simula la administracion de emails personales, se muestran los correos, se puede crear un email nuevo
y se puede eliminar uno de la lista de emails.

### Ejecución del proyecto

# clonar el proyecto

git clone https://gitlab.com/ejemplo..

# entrar en el directorio del proyecto

cd project-email-quasar

# instalar dependencias

npm install

# develop

quasar dev

Se requiere la version actual de node.js
